package com.lionseal.mm.viajantes;

import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.TableLayout;
import android.widget.TextView;

import com.lionseal.mm.viajantes.database.MyDBHelper;
import com.lionseal.mm.viajantes.database.Query;
import com.lionseal.mm.viajantes.utils.Utiles;

public class VerPedido extends Activity implements OnLongClickListener, OnClickListener,
		android.content.DialogInterface.OnClickListener {

	private String idespecial;
	private double total;
	private HashMap<String, Double> renglones;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ver_pedido);

		SharedPreferences settings = getSharedPreferences(AltaPedido.PREFS_ALTA_PEDIDO, 0);
		idespecial = settings.getString(AltaPedido.KEY_ID_ESPECIAL, null);
		if (idespecial == null) {
			// lel no podemos estar aca
			setResult(MainActivity.ERROR_CONEXION);
			finish();
		}
		SharedPreferences main = getSharedPreferences(MainActivity.PREFS_LOCALES, 0);
		TextView vendedor = (TextView) findViewById(R.id.text_ver_pedido_vendedor);
		TextView cliente = (TextView) findViewById(R.id.text_ver_pedido_cliente);
		vendedor.setText(main.getString(MainActivity.KEY_RAZON_SOCIAL_VENDEDOR, ""));
		cliente.setText(settings.getString(AltaPedido.KEY_RAZON_SOCIAL_CLIENTE, ""));
		renglones = new HashMap<String, Double>();

		cargarVentasRenglon();
	}

	public void buscarMas(View v) {
		setResult(0);
		finish();
	}

	public void confirmarPedido(View v) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle("Confirmar Pedido...");
		alertDialog.setMessage("Esta seguro que desea confirmar el pedido?");
		alertDialog.setIcon(android.R.drawable.ic_menu_save);
		alertDialog.setPositiveButton("Si", this);
		alertDialog.setNegativeButton("No", this);
		alertDialog.show();
	}

	private void cargarVentasRenglon() {
		TableLayout table = (TableLayout) findViewById(R.id.layout_table);
		View row;

		MyDBHelper helper = new MyDBHelper(this);
		SQLiteDatabase db = helper.getReadableDatabase();
		String query = Query.get().ventasRenglon() + " WHERE " + MyDBHelper.VentasRenglon.idventa + "='" + idespecial + "'";
		Cursor c = db.rawQuery(query, null);
		double sumaSubtotales = 0;
		while (c.moveToNext()) {
			row = getLayoutInflater().inflate(R.layout.ver_pedido_row, null);
			TextView descripcion = (TextView) row.findViewById(R.id.text_ver_pedido_descripcion);
			TextView id = (TextView) row.findViewById(R.id.text_ver_pedido_id);
			TextView cantidad = (TextView) row.findViewById(R.id.text_ver_pedido_cantidad);
			TextView subtotal = (TextView) row.findViewById(R.id.text_ver_pedido_subtotal);
			TextView idOculta = (TextView) row.findViewById(R.id.text_ver_pedido_id_oculta);

			String idrenglon = c.getString(c.getColumnIndex(MyDBHelper.VentasRenglon.id));
			double subtot = c.getDouble(c.getColumnIndex(MyDBHelper.VentasRenglon.subtotal));
			sumaSubtotales += subtot;

			descripcion.setText(c.getString(c.getColumnIndex(MyDBHelper.VentasRenglon.descripcion)));
			id.setText(c.getString(c.getColumnIndex(MyDBHelper.VentasRenglon.idarticulo)));
			cantidad.setText(c.getString(c.getColumnIndex(MyDBHelper.VentasRenglon.cantidad)));
			subtotal.setText(Utiles.parsearDouble(subtot));
			idOculta.setText(idrenglon);

			table.addView(row);
			row.setOnLongClickListener(this);
			row.setOnClickListener(this);
			row.setClickable(true);
			renglones.put(idrenglon, subtot);
		}

		c = db.rawQuery(Query.get().ventas(idespecial), null);
		if (c.moveToFirst()) {
			total = c.getDouble(c.getColumnIndex(MyDBHelper.Ventas.total));
			if (Double.compare(sumaSubtotales, total) > 0) {
				total = sumaSubtotales;
			}
		}
		actualizarTotal();

		db.close();
	}

	public void borrarRenglonPedido(View vg) {
		if (vg == null) {
			return;
		}

		TextView idOculta = (TextView) vg.findViewById(R.id.text_ver_pedido_id_oculta);
		String id = idOculta.getText().toString();

		MyDBHelper helper = new MyDBHelper(this);
		SQLiteDatabase db = helper.getWritableDatabase();
		String where = MyDBHelper.VentasRenglon.id + "=" + id;
		where += " AND " + MyDBHelper.VentasRenglon.idventa + "='" + idespecial + "'";
		db.delete(MyDBHelper.VentasRenglon.TABLE_NAME, where, null);

		String query = Query.get().ventasRenglon() + " WHERE " + MyDBHelper.VentasRenglon.idventa + "='" + idespecial + "'";
		Cursor c = db.rawQuery(query, null);
		double sumaSubtotales = 0;
		db.beginTransaction();
		while (c.moveToNext()) {
			sumaSubtotales = c.getDouble(c.getColumnIndex(MyDBHelper.VentasRenglon.subtotal));
		}
		db.setTransactionSuccessful();
		db.endTransaction();		

		total = sumaSubtotales;
		if (total < 0.01) {
			total = 0;
		}		
		ContentValues values = new ContentValues();
		values.put(MyDBHelper.Ventas.total, total);
		where = MyDBHelper.Ventas.idespecial + "='" + idespecial + "'";
		db.update(MyDBHelper.Ventas.TABLE_NAME, values, where, null);
		db.close();
		
		renglones.remove(id);
		actualizarTotal();

		vg.setVisibility(View.GONE);
	}

	private void actualizarTotal() {
		TextView tot = (TextView) findViewById(R.id.text_total);
		tot.setText(Utiles.parsearDouble(total));
	}

	private void cerrarPedido() {
		MyDBHelper helper = new MyDBHelper(this);
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(MyDBHelper.Ventas.vuelto, AltaPedido.KEY_PEDIDO_CERRADO);
		String where = MyDBHelper.Ventas.idespecial + "='" + idespecial + "'";
		db.update(MyDBHelper.Ventas.TABLE_NAME, values, where, null);
		db.close();

		SharedPreferences settings = getSharedPreferences(AltaPedido.PREFS_ALTA_PEDIDO, 0);
		settings.edit().clear().commit();
		Utiles.toast(this, "Pedido guardado");
		setResult(1);
		finish();
	}

	@Override
	public boolean onLongClick(View v) {
		borrarRenglonPedido(v);
		return true;
	}

	@Override
	public void onClick(View v) {
		Utiles.toast(this, "Mantener seleccionado para remover");
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		switch (which) {
		case DialogInterface.BUTTON_POSITIVE:
			if (renglones.size() > 0) {
				cerrarPedido();
			} else {
				Utiles.toast(this, "Debe existir al menos un articulo");
			}
			break;
		case DialogInterface.BUTTON_NEGATIVE:
			break;
		default:
			break;
		}
	}

}
