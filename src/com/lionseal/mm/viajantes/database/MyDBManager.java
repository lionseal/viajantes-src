package com.lionseal.mm.viajantes.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import android.app.Activity;
import android.content.ContentValues;
import android.content.SharedPreferences;

import com.lionseal.mm.viajantes.MainActivity;

public class MyDBManager extends MyDBConnector {

	private String idvendedor;

	public MyDBManager(Activity a) {
		super(a);
	}

	public void actualizarBD() {
		SharedPreferences settings = activity.getSharedPreferences(MainActivity.PREFS_LOCALES, 0);
		idvendedor = settings.getString(MainActivity.KEY_ID_VENDEDOR, "");
		progressBarTitle = "Actualizando Base de Datos...";
		progressBarTotal = 6;
		createProgressBar();
		startConnection();
	}

	public int connectionStarted() throws SQLException {
		progressBarActual++;
		progressBarMessage = "Actualizando vendedor...";

		ResultSet rs;

		// crear y cargar db en android
		MyDBHelper helper = new MyDBHelper(activity);
		db = helper.getWritableDatabase();
		helper.onUpgrade(db, 0, 0);
		ContentValues values = new ContentValues();

		// vendedor
		rs = st.executeQuery(Query.get().vendedor(idvendedor));
		if (rs.next()) {
			values.put(MyDBHelper.Vendedor.id, rs.getString(MyDBHelper.Vendedor.id));
			values.put(MyDBHelper.Vendedor.idsucursal, rs.getString(MyDBHelper.Vendedor.idsucursal));
			String rsVendedor = rs.getString(MyDBHelper.Vendedor.razon_social);
			values.put(MyDBHelper.Vendedor.razon_social, rsVendedor);
			values.put(MyDBHelper.Vendedor.domicilio, rs.getString(MyDBHelper.Vendedor.domicilio));
			values.put(MyDBHelper.Vendedor.codigo_postal, rs.getString(MyDBHelper.Vendedor.codigo_postal));
			values.put(MyDBHelper.Vendedor.password, rs.getString(MyDBHelper.Vendedor.password));
			values.put(MyDBHelper.Vendedor.cuota, rs.getString(MyDBHelper.Vendedor.cuota));
			values.put(MyDBHelper.Vendedor.cuota_limite_semanal, rs.getString(MyDBHelper.Vendedor.cuota_limite_semanal));
			values.put(MyDBHelper.Vendedor.telefono01, rs.getString(MyDBHelper.Vendedor.telefono01));
			values.put(MyDBHelper.Vendedor.telefono02, rs.getString(MyDBHelper.Vendedor.telefono02));
			db.beginTransaction();
			db.insert(MyDBHelper.Vendedor.TABLE_NAME, null, values);
			db.setTransactionSuccessful();
			db.endTransaction();
			SharedPreferences settings = activity.getSharedPreferences(MainActivity.PREFS_LOCALES, 0);
			settings.edit().putString(MainActivity.KEY_RAZON_SOCIAL_VENDEDOR, rsVendedor).commit();
			progressBarActual++;
		} else {
			progressError = true;
			return MainActivity.VENDEDOR_INVALIDO;
		}

		// clientes
		db.beginTransaction();
		rs = st.executeQuery(Query.get().clientesXvendedor(idvendedor));
		progressBarMessage = "Actualizando clientes... ";
		while (rs.next()) {
			values = new ContentValues();
			values.put(MyDBHelper.Clientes.id, rs.getString(MyDBHelper.Clientes.id));
			values.put(MyDBHelper.Clientes.idespecial, rs.getString(MyDBHelper.Clientes.idespecial));
			values.put(MyDBHelper.Clientes.documento, rs.getString(MyDBHelper.Clientes.documento));
			values.put(MyDBHelper.Clientes.apellido, rs.getString(MyDBHelper.Clientes.apellido));
			values.put(MyDBHelper.Clientes.nombre, rs.getString(MyDBHelper.Clientes.nombre));
			values.put(MyDBHelper.Clientes.razon_social, rs.getString(MyDBHelper.Clientes.razon_social));
			values.put(MyDBHelper.Clientes.cuit, rs.getString(MyDBHelper.Clientes.cuit));
			values.put(MyDBHelper.Clientes.idvendedor, rs.getString(MyDBHelper.Clientes.idvendedor));
			values.put(MyDBHelper.Clientes.mail, rs.getString(MyDBHelper.Clientes.mail));
			db.insert(MyDBHelper.Clientes.TABLE_NAME, null, values);
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		progressBarActual++;

		// marcas
		db.beginTransaction();
		rs = st.executeQuery(Query.get().marcasXconfiguracion());
		progressBarMessage = "Actualizando marcas... ";
		while (rs.next()) {
			values = new ContentValues();
			values.put(MyDBHelper.Marcas.id, rs.getString(MyDBHelper.Marcas.id));
			values.put(MyDBHelper.Marcas.descripcion, rs.getString(MyDBHelper.Marcas.descripcion));
			db.insert(MyDBHelper.Marcas.TABLE_NAME, null, values);
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		progressBarActual++;

		// articulos
		db.beginTransaction();
		rs = st.executeQuery(Query.get().articulosXmarcas());
		progressBarMessage = "Actualizando articulos... ";
		while (rs.next()) {
			values = new ContentValues();
			values.put(MyDBHelper.Articulos.id, rs.getString(MyDBHelper.Articulos.id));
			values.put(MyDBHelper.Articulos.codigo_barra, rs.getString(MyDBHelper.Articulos.codigo_barra));
			values.put(MyDBHelper.Articulos.descripcion, rs.getString(MyDBHelper.Articulos.descripcion));
			values.put(MyDBHelper.Articulos.idmarca, rs.getString(MyDBHelper.Articulos.idmarca));
			values.put(MyDBHelper.Articulos.idagrupamiento, rs.getString(MyDBHelper.Articulos.idagrupamiento));
			values.put(MyDBHelper.Articulos.idproveedor, rs.getString(MyDBHelper.Articulos.idproveedor));
			values.put(MyDBHelper.Articulos.precio_costo, rs.getString(MyDBHelper.Articulos.precio_costo));
			db.insert(MyDBHelper.Articulos.TABLE_NAME, null, values);
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		progressBarActual++;

		// agrupamientos
		db.beginTransaction();
		rs = st.executeQuery(Query.get().agrupamientosXarticulos());
		progressBarMessage = "Actualizando agrupamientos... ";
		while (rs.next()) {
			values = new ContentValues();
			values.put(MyDBHelper.Agrupamientos.id, rs.getString(MyDBHelper.Agrupamientos.id));
			values.put(MyDBHelper.Agrupamientos.descripcion, rs.getString(MyDBHelper.Agrupamientos.descripcion));
			db.insert(MyDBHelper.Agrupamientos.TABLE_NAME, null, values);
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		progressBarActual++;
		return MainActivity.EXITO_ACTUALIZAR;
	}
}
