package com.lionseal.mm.viajantes.database;

public final class Query {

	private static final Query instance = new Query();

	public static Query get() {
		return instance;
	}

	public String vendedor(String valor) {
		return "SELECT * FROM " + MyDBHelper.Vendedor.TABLE_NAME + " WHERE " + MyDBHelper.Vendedor.id + "=" + valor;
	}

	public String clientes(String valor) {
		return "SELECT * FROM " + MyDBHelper.Clientes.TABLE_NAME + " WHERE " + MyDBHelper.Clientes.idvendedor + "=" + valor;
	}

	public String clientes() {
		return "SELECT * FROM " + MyDBHelper.Clientes.TABLE_NAME;
	}

	public String clientesXvendedor(String idvendedor) {
		return clientes() + " WHERE " + MyDBHelper.Clientes.idvendedor + "=" + idvendedor;
	}

	public String articulosXconfiguracion(String valor) {
		return "SELECT * FROM " + MyDBHelper.Articulos.TABLE_NAME + ", " + MyDBHelper.Configuracion.TABLE_NAME + " WHERE "
				+ MyDBHelper.Articulos.idmarca + " = " + MyDBHelper.Configuracion.valor;
	}

	public String marcas() {
		return "SELECT * FROM " + MyDBHelper.Marcas.TABLE_NAME;
	}

	public String marcasXconfiguracion() {
		return "SELECT * FROM " + MyDBHelper.Marcas.TABLE_NAME + " WHERE " + MyDBHelper.Marcas.id + " IN ("
				+ configuracionValor() + ")";
	}

	public String marcas_idXconfiguracion() {
		return "SELECT " + MyDBHelper.Marcas.id + " FROM " + MyDBHelper.Marcas.TABLE_NAME + " WHERE " + MyDBHelper.Marcas.id
				+ " IN (" + configuracionValor() + ")";
	}

	public String agrupamientos() {
		return "SELECT * FROM " + MyDBHelper.Agrupamientos.TABLE_NAME;
	}

	public String agrupamientosXarticulos() {
		return "SELECT * FROM " + MyDBHelper.Agrupamientos.TABLE_NAME + " WHERE " + MyDBHelper.Agrupamientos.id + " IN ("
				+ articulos_idagrupamientoXmarcas() + ")";
	}

	public String articulos() {
		return "SELECT * FROM " + MyDBHelper.Articulos.TABLE_NAME;
	}

	public String articulosXmarcas() {
		return "SELECT * FROM " + MyDBHelper.Articulos.TABLE_NAME + " WHERE " + MyDBHelper.Articulos.idmarca + " IN ("
				+ marcas_idXconfiguracion() + ")";
	}

	public String articulos_idagrupamientoXmarcas() {
		return "SELECT " + MyDBHelper.Articulos.idagrupamiento + " FROM " + MyDBHelper.Articulos.TABLE_NAME + " WHERE "
				+ MyDBHelper.Articulos.idmarca + " IN (" + marcas_idXconfiguracion() + ")";
	}

	public String configuracionValor() {
		return "SELECT " + MyDBHelper.Configuracion.valor + " FROM " + MyDBHelper.Configuracion.TABLE_NAME;
	}

	public String ventas_vuelto(double sinTransferir) {
		return "SELECT * FROM " + MyDBHelper.Ventas.TABLE_NAME + " WHERE " + MyDBHelper.Ventas.vuelto + "=" + sinTransferir;
	}

	public String ventas(String idespecial) {
		return "SELECT * FROM " + MyDBHelper.Ventas.TABLE_NAME + " WHERE " + MyDBHelper.Ventas.idespecial + "='" + idespecial
				+ "'";
	}

	public String ventasRenglon() {
		return "SELECT * FROM " + MyDBHelper.VentasRenglon.TABLE_NAME;
	}

	public String deleteRenglonVentas(String id) {
		return "DELETE FROM " + MyDBHelper.VentasRenglon.TABLE_NAME + " WHERE " + MyDBHelper.VentasRenglon.id + "=" + id;
	}

	public String insertVentas() {
		return "INSERT INTO " + MyDBHelper.Ventas.TABLE_NAME + " (" + MyDBHelper.Ventas.idespecial + ","
				+ MyDBHelper.Ventas.fecha + "," + MyDBHelper.Ventas.hora + "," + MyDBHelper.Ventas.idcliente + ","
				+ MyDBHelper.Ventas.idvendedor + "," + MyDBHelper.Ventas.total + "," + MyDBHelper.Ventas.vuelto + ","
				+ MyDBHelper.Ventas.tipo + "," + MyDBHelper.Ventas.baja + "," + MyDBHelper.Ventas.comprobante + ") VALUES ";
	}

	public String insertVentasRenglon() {
		return "INSERT INTO " + MyDBHelper.VentasRenglon.TABLE_NAME + " (" + MyDBHelper.VentasRenglon.idventa + ","
				+ MyDBHelper.VentasRenglon.idarticulo + "," + MyDBHelper.VentasRenglon.precio_unitario + ","
				+ MyDBHelper.VentasRenglon.cantidad + "," + MyDBHelper.VentasRenglon.subtotal + ","
				+ MyDBHelper.VentasRenglon.descripcion + ") VALUES ";
	}
	
	public String updateClientesMail() {
		return "UPDATE " + MyDBHelper.Clientes.TABLE_NAME + " SET " + MyDBHelper.Clientes.mail + "=";
	}
}
