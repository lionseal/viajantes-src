package com.lionseal.mm.viajantes.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class MyData extends SQLiteOpenHelper {

	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "data";

	public MyData(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_DATOS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DROP_DATOS);

		onCreate(db);
	}

	public static final String SQL_CREATE_DATOS = 
			"CREATE TABLE "	+ Datos.TABLE_NAME + " ("
			+ Datos.id	+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ Datos.ip	+ " VARCHAR(200) NOT NULL," 
			+ Datos.puerto	+ " VARCHAR(200) NOT NULL," 
			+ Datos.nombre_bd + " VARCHAR(200) NOT NULL,"
			+ Datos.usuario + " VARCHAR(200) NOT NULL,"
			+ Datos.password + " VARCHAR(200) NOT NULL)";

	public static final String SQL_DROP_DATOS = "DROP TABLE IF EXISTS " + Datos.TABLE_NAME;

	public static abstract class Datos implements BaseColumns {
		public static final String TABLE_NAME = "datos";
		public static final String id = "id";
		public static final String ip = "ip";
		public static final String puerto = "puerto";
		public static final String nombre_bd = "nombre_bd";
		public static final String usuario = "usuario";
		public static final String password = "password";
	}

}
