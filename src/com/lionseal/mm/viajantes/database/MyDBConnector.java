package com.lionseal.mm.viajantes.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.lionseal.mm.viajantes.MainActivity;
import com.lionseal.mm.viajantes.database.MyData.Datos;

public abstract class MyDBConnector implements OnCancelListener {

	protected Activity activity;
	private String ip, puerto, nombreBD, usuario, password, urlServidor;
	protected SQLiteDatabase db;
	protected Connection con;
	protected Statement st;

	protected boolean progressIncomplete, progressError;
	protected String progressBarTitle, progressBarMessage = "Conectando con Base de Datos...";
	protected int progressBarTotal, progressBarActual = 0;

	public MyDBConnector(Activity a) {
		activity = a;
	}

	public void startConnection() {
		MyData helper = new MyData(activity);
		SQLiteDatabase db = helper.getWritableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM " + Datos.TABLE_NAME, null);
		if (c.moveToFirst()) {
			ip = c.getString(c.getColumnIndex(Datos.ip));
			puerto = c.getString(c.getColumnIndex(Datos.puerto));
			nombreBD = c.getString(c.getColumnIndex(Datos.nombre_bd));
			usuario = c.getString(c.getColumnIndex(Datos.usuario));
			password = c.getString(c.getColumnIndex(Datos.password));
			urlServidor = "jdbc:mysql://" + ip + ":" + puerto + "/" + nombreBD;

			new Conectar().execute(urlServidor, usuario, password);
		}
		db.close();
	}

	public void stopConnection() {
		try {
			if (st != null) {
				st.close();
			}
			if (con != null) {
				con.close();
			}
			if (db != null) {
				db.close();
			}
		} catch (Exception e) {
			Log.d("Error", "stopConnection" + e.getMessage());
		}
	}

	public abstract int connectionStarted() throws SQLException;

	private class Conectar extends AsyncTask<String, String, Integer> {
		protected Integer doInBackground(String... params) {
			Integer result;
			try {
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				con = DriverManager.getConnection(params[0], params[1], params[2]);
				st = con.createStatement();
				result = connectionStarted();
			} catch (Exception e) {
				Log.d("ConnectionError", "Error de conexi�n. Mensaje: " + e.getMessage());
				e.printStackTrace();
				result = MainActivity.ERROR_CONEXION;
				progressError = true;
			}
			return result;
		}

		protected void onPostExecute(Integer result) {
			switch (result) {
			case MainActivity.EXITO_ACTUALIZAR:
				((MainActivity) activity).actualizacionExitosa();
				progressBarMessage = "Base de Datos actualizada.";
				progressIncomplete = false;
				break;
			case MainActivity.EXITO_TRANSFERENCIA:
				((MainActivity) activity).transferenciaExitosa();
				progressBarMessage = "Transferencia completada.";
				progressIncomplete = false;
				break;
			default:
				((MainActivity) activity).bdError(result);
				break;
			}
			stopConnection();
		}
	}

	protected void createProgressBar() {
		final Handler progressBarHandler = new Handler();
		final ProgressDialog progressBar = new ProgressDialog(activity);

		// estado
		progressIncomplete = true;
		progressError = false;

		progressBar.setCancelable(true);
		progressBar.setMessage(progressBarMessage);
		progressBar.setTitle(progressBarTitle);
		progressBar.setMax(progressBarTotal);
		progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressBar.setOnCancelListener(this);
		progressBar.show();

		new Thread(new Runnable() {
			public void run() {
				while (progressIncomplete && !progressError) {
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					// Update the progress bar
					progressBarHandler.post(new Runnable() {
						public void run() {
							progressBar.setMessage(progressBarMessage);
							progressBar.setProgress(progressBarActual);
							progressBar.setMax(progressBarTotal);
						}
					});
				}
				if (progressError) {
					progressBar.dismiss();
				}
				if (!progressIncomplete) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					progressBar.dismiss();
				}
			}
		}).start();
	}

	public void onCancel(DialogInterface dialog) {
		stopConnection();
		((MainActivity) activity).bdError(MainActivity.CANCELADO);
	}

}
