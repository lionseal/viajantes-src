package com.lionseal.mm.viajantes.database;

import java.sql.SQLException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;

import com.lionseal.mm.viajantes.AltaPedido;
import com.lionseal.mm.viajantes.MainActivity;
import com.lionseal.mm.viajantes.utils.SendMail;
import com.lionseal.mm.viajantes.utils.Utiles;
import com.lionseal.mm.viajantes.utils.Ventas;
import com.lionseal.mm.viajantes.utils.VentasRenglon;

public class MyDBTransferencia extends MyDBConnector {

	public MyDBTransferencia(Activity a) {
		super(a);
	}

	public void transferirPedidos() {
		progressBarTitle = "Transfiriendo pedidos...";
		progressBarTotal = 4;
		createProgressBar();
		startConnection();
	}

	public int connectionStarted() throws SQLException {
		progressBarActual++;
		MyDBHelper helper = new MyDBHelper(activity);
		db = helper.getReadableDatabase();

		// Carga los pedidos desde la BD local y datos para los correos
		progressBarMessage = "Cargando pedidos...";
		String query = Query.get().ventas_vuelto(AltaPedido.KEY_PEDIDO_CERRADO);
		Cursor c = db.rawQuery(query, null);
		ArrayList<Ventas> listaVentas = new ArrayList<Ventas>();
		ArrayList<String> toArr = new ArrayList<String>(), bodyArr = new ArrayList<String>();
		if (c.moveToFirst()) {
			c.moveToPrevious();
			while (c.moveToNext()) {
				Ventas v = new Ventas(c);
				// cargar mail
				query = Query.get().clientes() + " WHERE " + MyDBHelper.Clientes.idespecial + "=" + v.getIdcliente();
				Cursor m = db.rawQuery(query, null);
				String mail = "";
				if (m.moveToFirst()) {
					mail = m.getString(m.getColumnIndex(MyDBHelper.Clientes.mail));
					toArr.add("leon.isla.22@gmail.com");
					v.setMail(mail);
				}

				query = Query.get().ventasRenglon() + " WHERE " + MyDBHelper.VentasRenglon.idventa + "=" + v.getIdespecial();
				Cursor cc = db.rawQuery(query, null);
				String body = "To: " + mail + "\nDescripción\nCantidad | Precio \n";
				while (cc.moveToNext()) {
					VentasRenglon vr = new VentasRenglon(cc);
					v.addRenglon(vr);
					body += vr.getDescripcion() + "\n" + vr.getCantidad() + " | " + Utiles.parsearDouble(vr.getSubtotal())
							+ "\n\n";
				}
				listaVentas.add(v);
				body += "Total: " + Utiles.parsearDouble(v.getTotal());
				body += "\nLos precios pueden variar sin previo aviso.";
				bodyArr.add(body);
			}
		} else {
			progressError = true;
			return MainActivity.NO_HAY_PEDIDOS;
		}
		progressBarActual++;

		// Transfiere los pedidos a la BD remota
		progressBarMessage = "Transfiriendo pedidos...";
		for (Ventas v : listaVentas) {
			// update mail cliente
			query = Query.get().updateClientesMail() + v.getMail() + " WHERE " + MyDBHelper.Clientes.idespecial + "="
					+ v.getIdcliente();
			st.executeUpdate(query);
			
			// insert ventas
			query = Query.get().insertVentas() + v.getValues();
			st.executeUpdate(query);
			for (VentasRenglon vr : v.getRenglones()) {
				// insert ventas_renglon
				query = Query.get().insertVentasRenglon() + vr.getValues();
				st.executeUpdate(query);
			}

			String where = MyDBHelper.Ventas.idespecial + "=" + v.getIdespecial();
			ContentValues values = new ContentValues();
			values.put(MyDBHelper.Ventas.vuelto, v.getVuelto());
			db.update(MyDBHelper.Ventas.TABLE_NAME, values, where, null);
		}
		progressBarActual++;

		progressBarMessage = "Enviando correos de pedidos...";
		new SendMail(activity, toArr, bodyArr);
		progressBarActual++;
		return MainActivity.EXITO_TRANSFERENCIA;
	}

}
