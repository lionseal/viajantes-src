package com.lionseal.mm.viajantes.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class MyDBHelper extends SQLiteOpenHelper {

	public static final int DATABASE_VERSION = 10;
	public static final String DATABASE_NAME = "ghr";
	
	public MyDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}	

	private static final String SQL_DROP_VENDEDOR = "DROP TABLE IF EXISTS " + Vendedor.TABLE_NAME;
	
	private static final String SQL_CREATE_VENDEDOR = 
			"CREATE TABLE "	+ Vendedor.TABLE_NAME + " (" 
			+ Vendedor.id + " INTEGER PRIMARY KEY," 
			+ Vendedor.idsucursal + " TINYINT(3),"
			+ Vendedor.razon_social + " VARCHAR(60) NOT NULL,"
			+ Vendedor.domicilio + " VARCHAR(45)," 
			+ Vendedor.codigo_postal + " VARCHAR(45)," 
			+ Vendedor.password + " VARCHAR(45) NOT NULL,"
			+ Vendedor.cuota + " DOUBLE," 
			+ Vendedor.cuota_limite_semanal	+ " DOUBLE," 
			+ Vendedor.telefono01 + " VARCHAR(45),"
			+ Vendedor.telefono02 + " VARCHAR(45))";
	
	private static final String SQL_DROP_CLIENTES = "DROP TABLE IF EXISTS " + Clientes.TABLE_NAME;
	
	private static final String SQL_CREATE_CLIENTES = 
			"CREATE TABLE "	+ Clientes.TABLE_NAME + " (" 
			+ Clientes.id + " INTEGER PRIMARY KEY," 
			+ Clientes.idespecial + " VARCHAR(18) NOT NULL," 
			+ Clientes.documento + " VARCHAR(20)," 
			+ Clientes.apellido	+ " VARCHAR(30)," 
			+ Clientes.nombre + " VARCHAR(45),"
			+ Clientes.razon_social + " VARCHAR(75) NOT NULL,"
			+ Clientes.cuit + " VARCHAR(13),"
			+ Clientes.idvendedor + " INTEGER NOT NULL,"
			+ Clientes.mail + " VARCHAR(80))";
//			+ Clientes.idsucursal + " TINYINT,"
//			+ Clientes.domicilio + " VARCHAR(80) NOT NULL," 
//			+ Clientes.numero + " INTEGER," 
//			+ Clientes.manzana + " VARCHAR(5),"
//			+ Clientes.parcela + " VARCHAR(5)," 
//			+ Clientes.unidad_funcional	+ " VARCHAR(5)," 
//			+ Clientes.fecha_alta + " DATE NOT NULL," 
//			+ Clientes.idbarrio	+ " INTEGER," 
//			+ Clientes.codigo_postal + " VARCHAR(10) NOT NULL,"
//			+ Clientes.idcalle + " INTEGER," 
//			+ Clientes.idcondicioniva + " INTEGER NOT NULL," 
//			+ Clientes.idformapago + " INTEGER NOT NULL," 
//			+ Clientes.idzona + " INTEGER NOT NULL," 
//			+ Clientes.idramo + " INTEGER,"
//			+ Clientes.idcategoria + " INTEGER," 
//			+ Clientes.estado + " INTEGER," 
//			+ Clientes.idtransporte + " VARCHAR(18),"
//			+ Clientes.idlista + " INTEGER," 
//			+ Clientes.bonificacion	+ " DOUBLE," 
//			+ Clientes.percepcion + " DOUBLE," 
//			+ Clientes.especial	+ " TINYINT," 
//			+ Clientes.remito + " TINYINT,"
//			+ Clientes.estado_cuenta + " TINYINT," 
//			+ Clientes.observaciones + " VARCHAR(80),"
//			+ Clientes.idgrupo + " INTEGER)";

	private static final String SQL_DROP_MARCAS = "DROP TABLE IF EXISTS " + Marcas.TABLE_NAME;
	
	private static final String SQL_CREATE_MARCAS = 
			"CREATE TABLE "	+ Marcas.TABLE_NAME + " (" 
			+ Marcas.id + " INTEGER PRIMARY KEY,"
			+ Marcas.descripcion + " VARCHAR(45) NOT NULL)";

	private static final String SQL_DROP_AGRUPAMIENTOS = "DROP TABLE IF EXISTS " + Agrupamientos.TABLE_NAME;
	
	private static final String SQL_CREATE_AGRUPAMIENTOS = 
			"CREATE TABLE "	+ Agrupamientos.TABLE_NAME + " (" 
			+ Agrupamientos.id + " INTEGER PRIMARY KEY," 
			+ Agrupamientos.descripcion	+ " VARCHAR(45) NOT NULL)";

	private static final String SQL_DROP_CONFIGURACION = "DROP TABLE IF EXISTS " + Configuracion.TABLE_NAME;
	
	private static final String SQL_CREATE_CONFIGURACION =  
			"CREATE TABLE "	+ Configuracion.TABLE_NAME + " ("
			+ Configuracion.id	+ " INTEGER PRIMARY KEY,"
			+ Configuracion.seccion	+ " VARCHAR(45) NOT NULL," 
			+ Configuracion.entrada	+ " VARCHAR(150) NOT NULL," 
			+ Configuracion.valor + " VARCHAR(200) NOT NULL)";

	private static final String SQL_DROP_ARTICULOS = "DROP TABLE IF EXISTS " + Articulos.TABLE_NAME;
	
	private static final String SQL_CREATE_ARTICULOS =  
			"CREATE TABLE " + Articulos.TABLE_NAME + " ("
			+ Articulos.id + " INTEGER PRIMARY KEY,"
			+ Articulos.codigo_barra + " VARCHAR(45) NOT NULL,"
			+ Articulos.descripcion + " VARCHAR(150) NOT NULL,"
			+ Articulos.precio_costo + " DOUBLE,"
			+ Articulos.idagrupamiento + " INTEGER,"
			+ Articulos.idmarca + " INTEGER,"
			+ Articulos.idproveedor + " INTEGER)";
//			+ Articulos.modelo + " VARCHAR(45),"
//			+ Articulos.importe_flete + " DOUBLE,"
//			+ Articulos.importe_seguro + " DOUBLE,"
//			+ Articulos.importe_adicional_01 + " DOUBLE,"
//			+ Articulos.importe_adicional_02 + " DOUBLE,"
//			+ Articulos.servicio + " TINYINT,"
//			+ Articulos.moto + " TINYINT,"
//			+ Articulos.idproveedor_alfa + " VARCHAR(18) NOT NULL,"
//			+ Articulos.iva_distinto + " TINYINT,"
//			+ Articulos.stock_00 + " DOUBLE,"
//			+ Articulos.stock_01 + " DOUBLE,"
//			+ Articulos.stock_02 + " DOUBLE,"
//			+ Articulos.stock_03 + " DOUBLE,"
//			+ Articulos.stock_04 + " DOUBLE,"
//			+ Articulos.stock_05 + " DOUBLE,"
//			+ Articulos.stock_06 + " DOUBLE,"
//			+ Articulos.stock_07 + " DOUBLE,"
//			+ Articulos.stock_08 + " DOUBLE,"
//			+ Articulos.stock_09 + " DOUBLE,"
//			+ Articulos.stock_10 + " DOUBLE,"
//			+ Articulos.stock_deposito_01 + " DOUBLE,"
//			+ Articulos.stock_deposito_02 + " DOUBLE,"
//			+ Articulos.minimo + " DOUBLE,"
//			+ Articulos.maximo + " DOUBLE,"
//			+ Articulos.moneda + " VARCHAR(10),"
//			+ Articulos.tipo_descuento + " VARCHAR(14),"
//			+ Articulos.descuento + " DOUBLE,"
//			+ Articulos.descuento_02 + " DOUBLE,"
//			+ Articulos.descuento_03 + " DOUBLE,"
//			+ Articulos.descuento_04 + " DOUBLE,"
//			+ Articulos.descuento_05 + " DOUBLE,"
//			+ Articulos.descuento_06 + " DOUBLE,"
//			+ Articulos.descuento_07 + " DOUBLE,"
//			+ Articulos.descuento_08 + " DOUBLE,"
//			+ Articulos.descuento_09 + " DOUBLE,"
//			+ Articulos.descuento_maximo + " DOUBLE,"
//			+ Articulos.fecha_alta + " DATE,"
//			+ Articulos.hora_alta + " TIME,"
//			+ Articulos.fecha_ultima_compra + " DATE,"
//			+ Articulos.fecha_ultima_venta + " DATE,"
//			+ Articulos.fecha_actualizacion_costo + " DATE,"
//			+ Articulos.especial + " TINYINT,"
//			+ Articulos.peso + " DOUBLE,"
//			+ Articulos.comision_directa + " DOUBLE,"
//			+ Articulos.comision_indirecta + " DOUBLE,"
//			+ Articulos.codigo_interno_proveedor + " VARCHAR(100),"
//			+ Articulos.idgrupo + " INTEGER,"
//			+ Articulos.borrado + " TINYINT)";
	
	private static final String SQL_DROP_VENTAS = "DROP TABLE IF EXISTS " + Ventas.TABLE_NAME;
	
	private static final String SQL_CREATE_VENTAS = 
			"CREATE TABLE " + Ventas.TABLE_NAME + " ("
			+ Ventas.id + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ Ventas.idespecial + " varchar(18) NOT NULL DEFAULT '',"
			+ Ventas.fecha + " date NOT NULL DEFAULT '0000-00-00',"
			+ Ventas.hora + " time DEFAULT '00:00:00',"
			+ Ventas.idcliente + " varchar(18) DEFAULT '0',"
			+ Ventas.idvendedor + " int(10)  NOT NULL DEFAULT '0',"
			+ Ventas.total + " double DEFAULT '0',"
			+ Ventas.vuelto + " double DEFAULT '0',"
			+ Ventas.tipo + " varchar(1) NOT NULL DEFAULT '',"
			+ Ventas.baja + " varchar(1) DEFAULT '',"
			+ Ventas.comprobante + " varchar(20) NOT NULL DEFAULT '')";
//			+ Ventas.idsucursal + " tinyint(3)  DEFAULT '0',"
//			+ Ventas.prefijo + " int(10)  DEFAULT '0',"
//			+ Ventas.numero + " int(10)  DEFAULT '0',"
//			+ Ventas.pedido_numero + " varchar(18) DEFAULT '',"
//			+ Ventas.idvendedor_preparo + " int(10)  DEFAULT NULL,"
//			+ Ventas.idcondicionventa + " int(10)  NOT NULL DEFAULT '0',"
//			+ Ventas.idcondicionpago + " int(10)  NOT NULL DEFAULT '0',"
//			+ Ventas.neto + " double DEFAULT '0',"
//			+ Ventas.inscripto_uno + " double DEFAULT '0',"
//			+ Ventas.inscripto_dos + " double DEFAULT '0',"
//			+ Ventas.valor_iva + " double DEFAULT '0',"
//			+ Ventas.retencion_iva + " double DEFAULT '0',"
//			+ Ventas.valor_ib + " double DEFAULT '0',"
//			+ Ventas.retencion_ib + " double DEFAULT '0',"
//			+ Ventas.valor_in + " double DEFAULT '0',"
//			+ Ventas.retencion_in + " double DEFAULT '0',"
//			+ Ventas.saldo + " double DEFAULT '0',"
//			+ Ventas.adelanto + " double DEFAULT '0',"
//			+ Ventas.efectivo + " double DEFAULT '0',"
//			+ Ventas.cheque + " double DEFAULT '0',"
//			+ Ventas.tarjeta + " double DEFAULT '0',"
//			+ Ventas.mutual + " double DEFAULT '0',"
//			+ Ventas.financiera + " double DEFAULT '0',"
//			+ Ventas.cuenta_corriente + " double DEFAULT '0',"
//			+ Ventas.saldo_favor + " double DEFAULT '0',"
//			+ Ventas.otras_monedas + " double DEFAULT NULL,"
//			+ Ventas.dolar + " double DEFAULT '0',"
//			+ Ventas.euro + " double DEFAULT '0',"
//			+ Ventas.cuenta + " tinyint(3)  DEFAULT '0',"
//			+ Ventas.tipo_comision + " tinyint(3)  DEFAULT NULL,"
//			+ Ventas.idgrupo + " int(10)  DEFAULT NULL)";
	
	private static final String SQL_DROP_VENTAS_RENGLON = "DROP TABLE IF EXISTS " + VentasRenglon.TABLE_NAME;
	
	private static final String SQL_CREATE_VENTAS_RENGLON = 
			"CREATE TABLE " + VentasRenglon.TABLE_NAME + " ("
			+ VentasRenglon.id + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ VentasRenglon.idventa + " varchar(18) NOT NULL DEFAULT '0',"
			+ VentasRenglon.idarticulo + " varchar(45) DEFAULT '',"
			+ VentasRenglon.precio_unitario + " double DEFAULT '0',"
			+ VentasRenglon.cantidad + " double DEFAULT '0',"
			+ VentasRenglon.subtotal + " double DEFAULT '0',"
			+ VentasRenglon.descripcion + " varchar(150) DEFAULT NULL)";
//			+ VentasRenglon.idcondicionventa + " int(10)  NOT NULL DEFAULT '0',"
//			+ VentasRenglon.idlista + " int(10)  NOT NULL DEFAULT '0',"
//			+ VentasRenglon.precio_costo + " double DEFAULT '0',"
//			+ VentasRenglon.pendiente + " double DEFAULT '0',"
//			+ VentasRenglon.descuento_adicional_tipo + " varchar(10) DEFAULT NULL,"
//			+ VentasRenglon.descuento_adicional + " double DEFAULT '0',"
//			+ VentasRenglon.bonificacion + " double DEFAULT '0',"
//			+ VentasRenglon.oferta_tipo + " varchar(10) DEFAULT '',"
//			+ VentasRenglon.oferta_valor + " double DEFAULT '0',"
//			+ VentasRenglon.valor_iva + " double DEFAULT '0',"
//			+ VentasRenglon.importe_iva + " double DEFAULT '0',"
//			+ VentasRenglon.estado + " int(10)  DEFAULT '0',"
//			+ VentasRenglon.id_opcion_01 + " varchar(45) DEFAULT NULL,"
//			+ VentasRenglon.id_opcion_02 + " varchar(45) DEFAULT NULL,"
//			+ VentasRenglon.id_opcion_03 + " varchar(45) DEFAULT NULL)";
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_VENDEDOR);
		db.execSQL(SQL_CREATE_CLIENTES);
		db.execSQL(SQL_CREATE_MARCAS);
		db.execSQL(SQL_CREATE_AGRUPAMIENTOS);
		db.execSQL(SQL_CREATE_CONFIGURACION);
		db.execSQL(SQL_CREATE_ARTICULOS);
		db.execSQL(SQL_CREATE_VENTAS);
		db.execSQL(SQL_CREATE_VENTAS_RENGLON);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DROP_VENDEDOR);
		db.execSQL(SQL_DROP_CLIENTES);
		db.execSQL(SQL_DROP_MARCAS);
		db.execSQL(SQL_DROP_AGRUPAMIENTOS);
		db.execSQL(SQL_DROP_CONFIGURACION);
		db.execSQL(SQL_DROP_ARTICULOS);
		db.execSQL(SQL_DROP_VENTAS);
		db.execSQL(SQL_DROP_VENTAS_RENGLON);
		onCreate(db);
	}

	public static abstract class Vendedor implements BaseColumns {
		public static final String TABLE_NAME = "vendedor";
		public static final String id = "id";
		public static final String idsucursal = "idsucursal";
		public static final String razon_social = "razon_social";
		public static final String password = "password";
		
		public static final String domicilio = "domicilio";
		public static final String codigo_postal = "codigo_postal";
		public static final String cuota = "cuota";
		public static final String cuota_limite_semanal = "cuota_limite_semanal";
		public static final String telefono01 = "telefono01";
		public static final String telefono02 = "telefono02";
	}

	public static abstract class Clientes implements BaseColumns {
		public static final String TABLE_NAME = "clientes";
		public static final String id = "id";
		public static final String idespecial = "idespecial";
		public static final String documento = "documento";
		public static final String apellido = "apellido";
		public static final String nombre = "nombre";
		public static final String razon_social = "razon_social";
		public static final String cuit = "cuit";
		public static final String idvendedor = "idvendedor";
		public static final String mail = "mail";
		
//		public static final String idsucursal = "idsucursal";
//		public static final String domicilio = "domicilio";
//		public static final String numero = "numero";
//		public static final String manzana = "manzana";
//		public static final String parcela = "parcela";
//		public static final String unidad_funcional = "unidad_funcional";
//		public static final String fecha_alta = "fecha_alta";
//		public static final String idbarrio = "idbarrio";
//		public static final String codigo_postal = "codigo_postal";
//		public static final String idcalle = "idcalle";
//		public static final String idcondicioniva = "idcondicioniva";
//		public static final String idformapago = "idformapago";
//		public static final String idzona = "idzona";
//		public static final String idramo = "idramo";
//		public static final String idcategoria = "idcategoria";
//		public static final String estado = "estado";
//		public static final String idtransporte = "idtransporte";
//		public static final String idlista = "idlista";
//		public static final String bonificacion = "bonificacion";
//		public static final String percepcion = "percepcion";
//		public static final String especial = "especial";
//		public static final String remito = "remito";
//		public static final String estado_cuenta = "estado_cuenta";
//		public static final String observaciones = "observaciones";
//		public static final String idgrupo = "idgrupo";
	}

	public static abstract class Marcas implements BaseColumns {
		public static final String TABLE_NAME = "marcas";
		public static final String id = "id";
		public static final String descripcion = "descripcion";
	}

	public static abstract class Agrupamientos implements BaseColumns {
		public static final String TABLE_NAME = "agrupamientos";
		public static final String id = "id";
		public static final String descripcion = "descripcion";
	}

	public static abstract class Configuracion implements BaseColumns {
		public static final String TABLE_NAME = "configuracion";
		public static final String id = "id";
		public static final String seccion = "seccion";
		public static final String entrada = "entrada";
		public static final String valor = "valor";
	}

	public static abstract class Articulos implements BaseColumns {
		public static final String TABLE_NAME = "articulos";
		public static final String id = "id";
		public static final String codigo_barra = "codigo_barra";
		public static final String descripcion = "descripcion";
		public static final String precio_costo = "precio_costo";
		public static final String idagrupamiento = "idagrupamiento";
		public static final String idmarca = "idmarca";
		public static final String idproveedor = "idproveedor";
		
//		public static final String modelo = "modelo";
//		public static final String importe_flete = "importe_flete";
//		public static final String importe_seguro = "importe_seguro";
//		public static final String importe_adicional_01 = "importe_adicional_01";
//		public static final String importe_adicional_02 = "importe_adicional_02";
//		public static final String servicio = "servicio";
//		public static final String moto = "moto";
//		public static final String idproveedor_alfa = "idproveedor_alfa";
//		public static final String iva_distinto = "iva_distinto";
//		public static final String stock_00 = "stock_00";
//		public static final String stock_01 = "stock_01";
//		public static final String stock_02 = "stock_02";
//		public static final String stock_03 = "stock_03";
//		public static final String stock_04 = "stock_04";
//		public static final String stock_05 = "stock_05";
//		public static final String stock_06 = "stock_06";
//		public static final String stock_07 = "stock_07";
//		public static final String stock_08 = "stock_08";
//		public static final String stock_09 = "stock_09";
//		public static final String stock_10 = "stock_10";
//		public static final String stock_deposito_01 = "stock_deposito_01";
//		public static final String stock_deposito_02 = "stock_deposito_02";
//		public static final String minimo = "minimo";
//		public static final String maximo = "maximo";
//		public static final String moneda = "moneda";
//		public static final String tipo_descuento = "tipo_descuento";
//		public static final String descuento = "descuento";
//		public static final String descuento_02 = "descuento_02";
//		public static final String descuento_03 = "descuento_03";
//		public static final String descuento_04 = "descuento_04";
//		public static final String descuento_05 = "descuento_05";
//		public static final String descuento_06 = "descuento_06";
//		public static final String descuento_07 = "descuento_07";
//		public static final String descuento_08 = "descuento_08";
//		public static final String descuento_09 = "descuento_09";
//		public static final String descuento_maximo = "descuento_maximo";
//		public static final String fecha_alta = "fecha_alta";
//		public static final String hora_alta = "hora_alta";
//		public static final String fecha_ultima_compra = "fecha_ultima_compra";
//		public static final String fecha_ultima_venta = "fecha_ultima_venta";
//		public static final String fecha_actualizacion_costo = "fecha_actualizacion_costo";
//		public static final String especial = "especial";
//		public static final String peso = "peso";
//		public static final String comision_directa = "comision_directa";
//		public static final String comision_indirecta = "comision_indirecta";
//		public static final String codigo_interno_proveedor = "codigo_interno_proveedor";
//		public static final String idgrupo = "idgrupo";
//		public static final String borrado = "borrado";
	}

	public static abstract class Ventas implements BaseColumns{
		public static final String TABLE_NAME = "ventas";
		public static final String id = "id";
		public static final String idespecial = "idespecial";
		public static final String fecha = "fecha";
		public static final String hora = "hora";
		public static final String idcliente = "idcliente";
		public static final String idvendedor = "idvendedor";
		public static final String comprobante = "comprobante";
		public static final String tipo = "tipo";
		public static final String total = "total";
		public static final String vuelto = "vuelto";
		public static final String baja = "baja";
		
//		public static final String idsucursal = "idsucursal";
//		public static final String prefijo = "prefijo";
//		public static final String numero = "numero";
//		public static final String pedido_numero = "pedido_numero";
//		public static final String idvendedor_preparo = "idvendedor_preparo";
//		public static final String idcondicionventa = "idcondicionventa";
//		public static final String idcondicionpago = "idcondicionpago";
//		public static final String neto = "neto";
//		public static final String inscripto_uno = "inscripto_uno";
//		public static final String inscripto_dos = "inscripto_dos";
//		public static final String valor_iva = "valor_iva";
//		public static final String retencion_iva = "retencion_iva";
//		public static final String valor_ib = "valor_ib";
//		public static final String retencion_ib = "retencion_ib";
//		public static final String valor_in = "valor_in";
//		public static final String retencion_in = "retencion_in";
//		public static final String saldo = "saldo";
//		public static final String adelanto = "adelanto";
//		public static final String efectivo = "efectivo";
//		public static final String cheque = "cheque";
//		public static final String tarjeta = "tarjeta";
//		public static final String mutual = "mutual";
//		public static final String financiera = "financiera";
//		public static final String cuenta_corriente = "cuenta_corriente";
//		public static final String saldo_favor = "saldo_favor";
//		public static final String otras_monedas = "otras_monedas";
//		public static final String dolar = "dolar";
//		public static final String euro = "euro";
//		public static final String cuenta = "cuenta";
//		public static final String tipo_comision = "tipo_comision";
//		public static final String idgrupo = "idgrupo";
	}

	public static abstract class VentasRenglon implements BaseColumns{
		public static final String TABLE_NAME = "ventas_renglon";
		public static final String id = "id";
		public static final String idventa = "idventa";
		public static final String idarticulo = "idarticulo";
		public static final String precio_unitario = "precio_unitario";
		public static final String cantidad = "cantidad";
		public static final String subtotal = "subtotal";
		public static final String descripcion = "descripcion";
		
//		public static final String idcondicionventa = "idcondicionventa";
//		public static final String idlista = "idlista";
//		public static final String precio_costo = "precio_costo";
//		public static final String pendiente = "pendiente";
//		public static final String descuento_adicional_tipo = "descuento_adicional_tipo";
//		public static final String descuento_adicional = "descuento_adicional";
//		public static final String bonificacion = "bonificacion";
//		public static final String oferta_tipo = "oferta_tipo";
//		public static final String oferta_valor = "oferta_valor";
//		public static final String valor_iva = "valor_iva";
//		public static final String importe_iva = "importe_iva";
//		public static final String estado = "estado";
//		public static final String id_opcion_01 = "id_opcion_01";
//		public static final String id_opcion_02 = "id_opcion_02";
//		public static final String id_opcion_03 = "id_opcion_03";
	}
}
