package com.lionseal.mm.viajantes;

import android.app.Activity;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.lionseal.mm.viajantes.database.MyData;
import com.lionseal.mm.viajantes.database.MyData.Datos;

public class Configuracion extends Activity {

	private boolean configurar_origen_datos = true;
	private String idvendedor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_configuracion);

		setResult(0);
	}

	public void guardarDatos(View v) {
		// checkEdits();
		SharedPreferences settings = getSharedPreferences(MainActivity.PREFS_LOCALES, 0);
		SharedPreferences.Editor editor = settings.edit();
		EditText edit;
		String value;
		MyData helper = new MyData(this);
		SQLiteDatabase db = helper.getWritableDatabase();
		if (configurar_origen_datos) {
			ContentValues values = new ContentValues();
			// ip server
			edit = (EditText) findViewById(R.id.edit_ip_server);
			value = edit.getText().toString();
			values.put(Datos.ip, value);
			// puerto
			edit = (EditText) findViewById(R.id.edit_puerto);
			value = edit.getText().toString();
			values.put(Datos.puerto, value);
			// nombre bd
			edit = (EditText) findViewById(R.id.edit_nombre_bd);
			value = edit.getText().toString();
			values.put(Datos.nombre_bd, value);
			// usuario
			edit = (EditText) findViewById(R.id.edit_usuario);
			value = edit.getText().toString();
			values.put(Datos.usuario, value);
			// password
			edit = (EditText) findViewById(R.id.edit_password);
			value = edit.getText().toString();
			values.put(Datos.password, value);

			db.insert(Datos.TABLE_NAME, null, values);

			// bandera origen datos
			editor.putBoolean(MainActivity.KEY_BANDERA_ORIGEN_DATOS, false);
		}
		// id vendedor
		edit = (EditText) findViewById(R.id.edit_id_vendedor);
		value = edit.getText().toString();
		editor.putString(MainActivity.KEY_ID_VENDEDOR, value);

		if (!value.equals(idvendedor)) {
			editor.putString(MainActivity.KEY_RAZON_SOCIAL_VENDEDOR, "[Razon_Social_Vendedor]");
		}

		editor.commit();

		String show = "Datos guardados.";
		if (value.equals(MainActivity.KEY_BORRAR_DATOS)) {
			editor.clear().commit();
			show = "Datos borrados.";
			helper.onUpgrade(db, 0, 0);
		} else {
			setResult(1);
		}
		
		db.close();

		Toast.makeText(this, show, Toast.LENGTH_LONG).show();
		finish();
	}

	public void onResume() {
		super.onResume();

		SharedPreferences settings = getSharedPreferences(MainActivity.PREFS_LOCALES, 0);
		EditText edit;
		String value;
		configurar_origen_datos = settings.getBoolean(MainActivity.KEY_BANDERA_ORIGEN_DATOS, true);
		if (!configurar_origen_datos) {
			MyData helper = new MyData(this);
			SQLiteDatabase db = helper.getWritableDatabase();
			Cursor c = db.rawQuery("SELECT * FROM " + Datos.TABLE_NAME, null);
			if (c.moveToFirst()) {
				// ip server
				value = c.getString(c.getColumnIndex(Datos.ip));
				edit = (EditText) findViewById(R.id.edit_ip_server);
				edit.setText(value);
				edit.setEnabled(false);
				edit.setFocusable(false);
				edit.setFocusableInTouchMode(false);
				// puerto
				edit = (EditText) findViewById(R.id.edit_puerto);
				ViewGroup vg = (ViewGroup) edit.getParent();
				vg.removeView(edit);
				// nombre bd
				value = c.getString(c.getColumnIndex(Datos.nombre_bd));
				edit = (EditText) findViewById(R.id.edit_nombre_bd);
				edit.setText(value);
				edit.setEnabled(false);
				edit.setFocusable(false);
				edit.setFocusableInTouchMode(false);
				// ususario
				edit = (EditText) findViewById(R.id.edit_usuario);
				vg = (ViewGroup) edit.getParent();
				vg.removeView(edit);
				// password
				edit = (EditText) findViewById(R.id.edit_password);
				vg = (ViewGroup) edit.getParent();
				vg.removeView(edit);
			}
			db.close();
		}
		// id vendedor
		value = settings.getString(MainActivity.KEY_ID_VENDEDOR, "");
		edit = (EditText) findViewById(R.id.edit_id_vendedor);
		edit.setText(value);
		idvendedor = value;
	}
}
