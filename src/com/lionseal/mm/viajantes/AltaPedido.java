package com.lionseal.mm.viajantes;

import java.util.ArrayList;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.lionseal.mm.viajantes.database.MyDBHelper;
import com.lionseal.mm.viajantes.database.Query;
import com.lionseal.mm.viajantes.utils.SpinnerAdapter;
import com.lionseal.mm.viajantes.utils.SpinnerArticulos;
import com.lionseal.mm.viajantes.utils.SpinnerItem;
import com.lionseal.mm.viajantes.utils.SpinnerItemArticulo;
import com.lionseal.mm.viajantes.utils.Utiles;

public class AltaPedido extends Activity implements OnCheckedChangeListener, OnEditorActionListener {

	private final static int ACTIVITY_VER_PEDIDO = 100;

	private boolean esconderArticulos;
	private Spinner spinnerMarcas, spinnerAgrupamientos, spinnerArticulos;
	private RadioGroup radioGroup;
	private EditText editBuscar, cantidad;
	private MyDBHelper helper = new MyDBHelper(this);
	private String idespecial;
	private double total;

	public final static String PREFS_ALTA_PEDIDO = "prefsAltaPedido";
	public final static String KEY_MARCA_SELECTED = "marcaSelected";
	public final static String KEY_AGRUPAMIENTO_SELECTED = "agrupamientoSelected";
	public final static String KEY_BUSQUEDA_ARTICULO = "busquedaArticulo";
	public final static String KEY_ESCONDER_ARTICULOS = "esconderArticulos";
	public final static String KEY_ARTICULO_SELECTED = "articuloSelected";
	public final static String KEY_RADIO_BUTTON_ID = "radioButtonId";
	public final static String KEY_CLIENTE_SELECTED = "clienteSelected";
	public final static String KEY_ID_ESPECIAL = "idEspecial";
	public final static String KEY_BUSQUEDA_CLIENTE = "busquedaCliente";
	public final static String KEY_RAZON_SOCIAL_CLIENTE = "razonSocialCliente";
	public final static String KEY_MAIL_CLIENTE = "mailCliente";
	public final static double KEY_PEDIDO_ABIERTO = 1.0;
	public final static double KEY_PEDIDO_CERRADO = 2.0;
	public final static double KEY_PEDIDO_ENVIADO = 3.0;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alta_pedido);

		SharedPreferences settings = getSharedPreferences(PREFS_ALTA_PEDIDO, 0);
		idespecial = settings.getString(KEY_ID_ESPECIAL, null);
		if (idespecial == null) {
			// lel no podemos estar aca
			setResult(MainActivity.ERROR_CONEXION);
			finish();
		}

		spinnerMarcas = (Spinner) findViewById(R.id.spinner_marcas);
		spinnerAgrupamientos = (Spinner) findViewById(R.id.spinner_agrupamientos);
		spinnerArticulos = (Spinner) findViewById(R.id.spinner_articulos);
		editBuscar = (EditText) findViewById(R.id.edit_buscar_articulo);
		radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
		cantidad = (EditText) findViewById(R.id.edit_cantidad);
		loadListas();
		editHint();
		radioGroup.setOnCheckedChangeListener(this);
		editBuscar.setOnEditorActionListener(this);
		cantidad.setOnEditorActionListener(this);
		setResult(0);
	}

	private void loadListas() {
		SQLiteDatabase db = helper.getReadableDatabase();

		ArrayList<SpinnerItem> values;
		SpinnerItem item;
		Cursor c;

		// Marcas
		c = db.rawQuery(Query.get().marcas() + " ORDER BY descripcion", null);
		values = new ArrayList<SpinnerItem>();
		if (c.moveToFirst()) {
			c.moveToPrevious();
			values.add(new SpinnerItem("", "Seleccionar Marca", false));
			while (c.moveToNext()) {
				item = new SpinnerItem(c.getString(c.getColumnIndex(MyDBHelper.Marcas.id)), c.getString(
						c.getColumnIndex(MyDBHelper.Marcas.descripcion)).trim(), false);
				values.add(item);
			}
		} else {
			values.add(new SpinnerItem("", "Por favor actualice Base de datos", false));
		}
		SpinnerAdapter adapter = new SpinnerAdapter(this, R.layout.spinner_row, values);
		spinnerMarcas.setOnItemSelectedListener(adapter);
		spinnerMarcas.setAdapter(adapter);

		// Agrupamientos
		c = db.rawQuery(Query.get().agrupamientos() + " ORDER BY descripcion", null);
		values = new ArrayList<SpinnerItem>();
		if (c.moveToFirst()) {
			c.moveToPrevious();
			values.add(new SpinnerItem("", "Seleccionar Agrupamiento", false));
			while (c.moveToNext()) {
				item = new SpinnerItem(c.getString(c.getColumnIndex(MyDBHelper.Agrupamientos.id)), c.getString(
						c.getColumnIndex(MyDBHelper.Agrupamientos.descripcion)).trim(), false);
				values.add(item);
			}
		} else {
			values.add(new SpinnerItem("", "Por favor actualice Base de datos", false));
		}
		adapter = new SpinnerAdapter(this, R.layout.spinner_row, values);
		spinnerAgrupamientos.setOnItemSelectedListener(adapter);
		spinnerAgrupamientos.setAdapter(adapter);

		db.close();
	}

	private void editHint() {
		RadioButton rb = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
		editBuscar.setHint("Buscar por " + rb.getText().toString());
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		editHint();
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (actionId == 100) {
			realizarBusqueda(editBuscar);
			return true;
		}
		if (actionId == 200) {
			agregarArticulo(cantidad);
			return true;
		}
		return false;
	}

	public void realizarBusqueda(View view) {
		ArrayList<SpinnerItem> values;
		SpinnerItemArticulo item;
		Cursor c;

		String query = cargarQueryBusqueda();
		SQLiteDatabase db = helper.getReadableDatabase();

		c = db.rawQuery(query, null);
		values = new ArrayList<SpinnerItem>();
		if (c.moveToFirst()) {
			// Si se encuentra algun articulo segun query
			c.moveToPrevious();
			values.add(new SpinnerItemArticulo("", "Seleccionar Articulo", false, 0));
			while (c.moveToNext()) {
				item = new SpinnerItemArticulo(c.getString(c.getColumnIndex(MyDBHelper.Articulos.codigo_barra)), c.getString(
						c.getColumnIndex(MyDBHelper.Articulos.descripcion)).trim(), false, c.getDouble(c
						.getColumnIndex(MyDBHelper.Articulos.precio_costo)));
				values.add(item);
			}
			cantidad.setText("1");
			View v = findViewById(R.id.layout_linea_agregar);
			v.setVisibility(View.VISIBLE);
		} else {
			// No se encuentra ningun articulo
			values.add(new SpinnerItemArticulo("", "Ningun articulo encontrado", false, 0));
			View v = findViewById(R.id.layout_linea_agregar);
			v.setVisibility(View.INVISIBLE);
		}

		SpinnerArticulos adapter = new SpinnerArticulos(this, R.layout.spinner_row, values);
		spinnerArticulos.setOnItemSelectedListener(adapter);
		spinnerArticulos.setAdapter(adapter);
		spinnerArticulos.setVisibility(View.VISIBLE);
		Utiles.esconderTeclado(editBuscar);

		db.close();
		esconderArticulos = false;
	}

	public void agregarArticulo(View view) {
		SpinnerArticulos adapter = (SpinnerArticulos) spinnerArticulos.getAdapter();
		SpinnerItemArticulo item = (SpinnerItemArticulo) adapter.getItemSelected();

		if (item == null) {
			Toast.makeText(this, "Ning�n articulo seleccionado", Toast.LENGTH_SHORT).show();
			return;
		}

		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		double cant = Double.valueOf(cantidad.getText().toString());
		double subtotal = cant * item.getPrecio();

		values.put(MyDBHelper.VentasRenglon.idventa, idespecial);
		values.put(MyDBHelper.VentasRenglon.descripcion, item.getDescripcion());
		values.put(MyDBHelper.VentasRenglon.precio_unitario, item.getPrecio());
		values.put(MyDBHelper.VentasRenglon.cantidad, cant);
		values.put(MyDBHelper.VentasRenglon.subtotal, subtotal);
		values.put(MyDBHelper.VentasRenglon.idarticulo, item.getId());

		db.beginTransaction();
		db.insert(MyDBHelper.VentasRenglon.TABLE_NAME, null, values);
		db.setTransactionSuccessful();
		db.endTransaction();

		Cursor c = db.rawQuery(Query.get().ventas(idespecial), null);
		if (c.moveToFirst()) {
			total = c.getDouble(c.getColumnIndex(MyDBHelper.Ventas.total));
			total += subtotal;

			values = new ContentValues();
			values.put(MyDBHelper.Ventas.total, total);
			String where = MyDBHelper.Ventas.idespecial + "='" + idespecial + "'";
			db.update(MyDBHelper.Ventas.TABLE_NAME, values, where, null);
		}

		Utiles.toast(this, "Articulo a�adido: " + item.getDescripcion());

		spinnerArticulos.setVisibility(View.INVISIBLE);
		View v = findViewById(R.id.layout_linea_agregar);
		v.setVisibility(View.INVISIBLE);
		esconderArticulos = true;

		Utiles.esconderTeclado(cantidad);
		db.close();
	}

	private String cargarQueryBusqueda() {
		String query = Query.get().articulos();
		String conector = " WHERE ";

		// marcas
		SpinnerAdapter ad = (SpinnerAdapter) spinnerMarcas.getAdapter();
		SpinnerItem spiMarcas = ad.getItemSelected();
		if (spiMarcas != null) {
			query += conector + MyDBHelper.Articulos.idmarca + "=" + spiMarcas.getId();
			conector = " AND ";
		}

		// agrupamientos
		ad = (SpinnerAdapter) spinnerAgrupamientos.getAdapter();
		SpinnerItem spiAgrupamientos = ad.getItemSelected();
		if (spiAgrupamientos != null) {
			query += conector + MyDBHelper.Articulos.idagrupamiento + "=" + spiAgrupamientos.getId();
			conector = " AND ";
		}

		// codigo o descripcion edit busqueda + " ORDER BY descripcion"
		RadioButton rb = (RadioButton) radioGroup.findViewById(R.id.radio_codigo);
		if (rb.isChecked()) {
			query += conector + MyDBHelper.Articulos.codigo_barra + " LIKE '%" + editBuscar.getText().toString() + "%'"
					+ " ORDER BY " + MyDBHelper.Articulos.codigo_barra;
		} else {
			query += conector + MyDBHelper.Articulos.descripcion + " LIKE '%" + editBuscar.getText().toString() + "%'"
					+ " ORDER BY " + MyDBHelper.Articulos.descripcion;
		}

		return query;
	}

	public void verPedido(View view) {
		Intent intent = new Intent(this, VerPedido.class);
		startActivityForResult(intent, ACTIVITY_VER_PEDIDO);
	}

	public void continuarPedido(View view) {
	}

	protected void onPause() {
		super.onPause();

		SharedPreferences settings = getSharedPreferences(PREFS_ALTA_PEDIDO, 0);
		SharedPreferences.Editor editor = settings.edit();
		SpinnerAdapter adapter;

		// agrupamiento
		adapter = (SpinnerAdapter) spinnerAgrupamientos.getAdapter();
		editor.putInt(KEY_AGRUPAMIENTO_SELECTED, adapter.getPositionItemSelected());
		// marcas
		adapter = (SpinnerAdapter) spinnerMarcas.getAdapter();
		editor.putInt(KEY_MARCA_SELECTED, adapter.getPositionItemSelected());
		// articulo
		if (!esconderArticulos) {
			adapter = (SpinnerAdapter) spinnerArticulos.getAdapter();
			editor.putInt(KEY_ARTICULO_SELECTED, adapter.getPositionItemSelected());
		}
		editor.putBoolean(KEY_ESCONDER_ARTICULOS, esconderArticulos);
		editor.putString(KEY_BUSQUEDA_ARTICULO, editBuscar.getText().toString());
		editor.putInt(KEY_RADIO_BUTTON_ID, radioGroup.getCheckedRadioButtonId());

		editor.commit();
	}

	protected void onResume() {
		super.onResume();

		SharedPreferences settings = getSharedPreferences(PREFS_ALTA_PEDIDO, 0);

		// agrupamiento
		spinnerAgrupamientos.setSelection(settings.getInt(KEY_AGRUPAMIENTO_SELECTED, 0));
		SpinnerAdapter adapter = (SpinnerAdapter) spinnerAgrupamientos.getAdapter();
		adapter.setSelected(settings.getInt(KEY_AGRUPAMIENTO_SELECTED, 0), spinnerAgrupamientos);
		// marcas
		spinnerMarcas.setSelection(settings.getInt(KEY_MARCA_SELECTED, 0));
		adapter = (SpinnerAdapter) spinnerMarcas.getAdapter();
		adapter.setSelected(settings.getInt(KEY_MARCA_SELECTED, 0), spinnerMarcas);
		// edit busqueda
		editBuscar.setText(settings.getString(KEY_BUSQUEDA_ARTICULO, ""));
		// radio group
		RadioButton rb = (RadioButton) radioGroup.findViewById(settings.getInt(KEY_RADIO_BUTTON_ID, R.id.radio_codigo));
		rb.setChecked(true);
		// Articulos
		esconderArticulos = settings.getBoolean(KEY_ESCONDER_ARTICULOS, true);
		if (esconderArticulos) {
			spinnerArticulos.setVisibility(View.INVISIBLE);
			View v = findViewById(R.id.layout_linea_agregar);
			v.setVisibility(View.INVISIBLE);
		} else {
			realizarBusqueda(null);
			spinnerArticulos.setSelection(settings.getInt(KEY_ARTICULO_SELECTED, 0));
		}
	}

	public void onActivityResult(int code, int res, Intent data) {
		if (code == ACTIVITY_VER_PEDIDO) {
			if (res == 0) {
				return;
			}
			setResult(1);
			finish();
		}
	}
}
