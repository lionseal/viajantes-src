package com.lionseal.mm.viajantes.utils;

public class SpinnerItemArticulo extends SpinnerItem {
	public SpinnerItemArticulo(String i, String d, boolean s, double p) {
		super(i, d, s);
		setPrecio(p);
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	private double precio;
}
