package com.lionseal.mm.viajantes.utils;

import com.lionseal.mm.viajantes.database.MyDBHelper;

import android.database.Cursor;

public class VentasRenglon {
	private String idventa, idarticulo, descripcion;
	private double cantidad, precio, subtotal;

	public VentasRenglon(Cursor c) {
		setIdventa(c.getString(c.getColumnIndex(MyDBHelper.VentasRenglon.idventa)));
		setIdarticulo(c.getString(c.getColumnIndex(MyDBHelper.VentasRenglon.idarticulo)));
		setPrecio(c.getDouble(c.getColumnIndex(MyDBHelper.VentasRenglon.precio_unitario)));
		setCantidad(c.getDouble(c.getColumnIndex(MyDBHelper.VentasRenglon.cantidad)));
		setSubtotal(c.getDouble(c.getColumnIndex(MyDBHelper.VentasRenglon.subtotal)));
		setDescripcion(c.getString(c.getColumnIndex(MyDBHelper.VentasRenglon.descripcion)));
	}

	public String getValues() {
		return "(" + getIdventa() + "," + getIdarticulo() + "," + getPrecio() + "," + getCantidad() + "," + getSubtotal() + ","
				+ getDescripcion() + ")";
	}

	public String getDescripcion() {
		return "'" + descripcion + "'";
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getIdarticulo() {
		return "'" + idarticulo + "'";
	}

	public void setIdarticulo(String idarticulo) {
		this.idarticulo = idarticulo;
	}

	public String getIdventa() {
		return "'" + idventa + "'";
	}

	public void setIdventa(String idventa) {
		this.idventa = idventa;
	}

	public double getCantidad() {
		return cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}
}
