package com.lionseal.mm.viajantes.utils;

import java.util.ArrayList;

import android.app.Activity;
import android.util.Log;

public class SendMail {

	public SendMail(Activity a, ArrayList<String> toArr, ArrayList<String> bodyArr) {
		for (int i = 0; i < toArr.size(); i++) {
			Mail m = new Mail("leon.isla.22@gmail.com", "l35.e496.o695n");

			m.setTo(toArr.get(i));
			m.setFrom("leon.isla.22@gmail.com");
			m.setSubject("This is an email sent using my Mail JavaMail wrapper from an Android device.");
			m.setBody(bodyArr.get(i));

			try {
				m.send();
			} catch (Exception e) {
				Log.e("MailApp", "Could not send email", e);
			}
		}
	}
}