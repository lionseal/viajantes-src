package com.lionseal.mm.viajantes.utils;

import java.util.ArrayList;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.lionseal.mm.viajantes.R;

public class SpinnerArticulos extends SpinnerAdapter {

	public SpinnerArticulos(Activity a, int textViewResourceId, ArrayList d) {
		super(a, textViewResourceId, d);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int pos, long id) {
		super.onItemSelected(parent, v, pos, id);

		TextView precio = (TextView) activity.findViewById(R.id.text_precio_articulo);
		SpinnerItemArticulo item = (SpinnerItemArticulo) data.get(pos);

		if (pos == 0) {
			precio.setText("$0,00");
		} else {
			precio.setText(Utiles.parsearDouble(item.getPrecio()));
		}
	}
}
