package com.lionseal.mm.viajantes.utils;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.lionseal.mm.viajantes.R;

public class SpinnerAdapter extends ArrayAdapter<String> implements OnItemSelectedListener {

	protected Activity activity;
	protected ArrayList<SpinnerItem> data;
	protected static LayoutInflater inflater;
	protected int selected = 0;

	public SpinnerAdapter(Activity a, int textViewResourceId, ArrayList d) {
		super(a, textViewResourceId, d);
		activity = a;
		data = d;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	public View getCustomView(int position, View convertView, ViewGroup parent) {
		View row = inflater.inflate(R.layout.spinner_row, parent, false);
		SpinnerItem item = data.get(position);
		TextView descripcion = (TextView) row.findViewById(R.id.row_text_descripcion);
		TextView id = (TextView) row.findViewById(R.id.row_text_id);
		RadioButton rb = (RadioButton) row.findViewById(R.id.row_radio_button);
		if (position == 0) {
			descripcion.setText(item.getDescripcion());
			id.setText(item.getId());
			rb.setVisibility(View.GONE);
		} else {
			descripcion.setText(item.getDescripcion());
			id.setText("ID: " + item.getId());
			rb.setChecked(item.isSelected());
		}
		rb.setFocusable(false);
		rb.setFocusableInTouchMode(false);
		// rb.setEnabled(false);
		return row;
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int pos, long id) {
		if (v == null) {
			return;
		}
		SpinnerItem prevSelected = data.get(selected);
		prevSelected.setSelected(false);

		SpinnerItem newSelected = data.get(pos);
		newSelected.setSelected(true);
		RadioButton rb = (RadioButton) v.findViewById(R.id.row_radio_button);
		rb.setChecked(newSelected.isSelected());

		selected = pos;
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

	public SpinnerItem getItemSelected() {
		if (selected == 0) {
			return null;
		}
		return data.get(selected);
	}

	public int getPositionItemSelected() {
		return selected;
	}

	public void setSelected(int pos, ViewGroup p) {
		onItemSelected(null, getView(pos, null, p), pos, 0);
	}
}
