package com.lionseal.mm.viajantes.utils;

public class SpinnerItem {
	private String descripcion, id;
	private boolean selected;

	public SpinnerItem(String i, String d, boolean s){
		setDescripcion(d);
		setId(i);
		setSelected(s);
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
