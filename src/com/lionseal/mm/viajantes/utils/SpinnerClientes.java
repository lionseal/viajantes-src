package com.lionseal.mm.viajantes.utils;

import java.util.ArrayList;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;

import com.lionseal.mm.viajantes.SeleccionCliente;

public class SpinnerClientes extends SpinnerAdapter {

	public SpinnerClientes(Activity a, int textViewResourceId, ArrayList d) {
		super(a, textViewResourceId, d);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int pos, long id) {
		super.onItemSelected(parent, v, pos, id);

		SpinnerItemCliente item = (SpinnerItemCliente) data.get(pos);

		EditText e = ((SeleccionCliente) activity).editMail;
		if (pos == 0) {
			e.setText("");
			e.setVisibility(View.INVISIBLE);
		} else {
			e.setText(item.getMail());
			e.setVisibility(View.VISIBLE);
		}
	}
}
