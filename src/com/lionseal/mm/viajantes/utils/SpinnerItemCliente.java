package com.lionseal.mm.viajantes.utils;

public class SpinnerItemCliente extends SpinnerItem {

	private String mail;
	
	public SpinnerItemCliente(String i, String d, boolean s, String m) {
		super(i, d, s);
		setMail(m);
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

}
