package com.lionseal.mm.viajantes.utils;

import java.util.ArrayList;

import com.lionseal.mm.viajantes.AltaPedido;
import com.lionseal.mm.viajantes.database.MyDBHelper;

import android.database.Cursor;

public class Ventas {
	private ArrayList<VentasRenglon> renglones = new ArrayList<VentasRenglon>();
	private double total, vuelto;
	private String idespecial, idcliente, idvendedor, fecha, hora, comprobante, tipo, baja;
	private String mail;

	public Ventas(Cursor c) {
		setIdespecial(c.getString(c.getColumnIndex(MyDBHelper.Ventas.idespecial)));
		setFecha(c.getString(c.getColumnIndex(MyDBHelper.Ventas.fecha)));
		setHora(c.getString(c.getColumnIndex(MyDBHelper.Ventas.hora)));
		setIdcliente(c.getString(c.getColumnIndex(MyDBHelper.Ventas.idcliente)));
		setIdvendedor(c.getString(c.getColumnIndex(MyDBHelper.Ventas.idvendedor)));
		setTotal(c.getDouble(c.getColumnIndex(MyDBHelper.Ventas.total)));
		setVuelto(AltaPedido.KEY_PEDIDO_ENVIADO);
		setTipo("X");
		setBaja("");
		setComprobante("PEDIDO");
	}

	public String getValues() {
		return "(" + getIdespecial() + "," + getFecha() + "," + getHora() + "," + getIdcliente() + "," + getIdvendedor() + ","
				+ getTotal() + "," + getVuelto() + "," + getTipo() + "," + getBaja() + "," + getComprobante() + ")";
	}

	public String getBaja() {
		return "'" + baja + "'";
	}

	public void setBaja(String baja) {
		this.baja = baja;
	}

	public String getTipo() {
		return "'" + tipo + "'";
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getComprobante() {
		return "'" + comprobante + "'";
	}

	public void setComprobante(String comprobante) {
		this.comprobante = comprobante;
	}

	public String getHora() {
		return "'" + hora + "'";
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getFecha() {
		return "'" + fecha + "'";
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getIdvendedor() {
		return "'" + idvendedor + "'";
	}

	public void setIdvendedor(String idvendedor) {
		this.idvendedor = idvendedor;
	}

	public String getIdcliente() {
		return "'" + idcliente + "'";
	}

	public void setIdcliente(String idcliente) {
		this.idcliente = idcliente;
	}

	public String getIdespecial() {
		return "'" + idespecial + "'";
	}

	public void setIdespecial(String idespecial) {
		this.idespecial = idespecial;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public VentasRenglon getRenglon(int index) {
		return renglones.get(index);
	}

	public ArrayList<VentasRenglon> getRenglones() {
		return renglones;
	}

	public void addRenglon(VentasRenglon renglon) {
		this.renglones.add(renglon);
	}

	public double getVuelto() {
		return vuelto;
	}

	public void setVuelto(double vuelto) {
		this.vuelto = vuelto;
	}

	public String getMail() {
		return "'" + mail + "'";
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
}
