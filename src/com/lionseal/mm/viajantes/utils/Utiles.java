package com.lionseal.mm.viajantes.utils;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

public class Utiles {

	public static String parsearDouble(double d) {
		String p[] = (d + "").split("\\.");
		p[1] += "00";
		return "$" + p[0] + "," + p[1].charAt(0) + p[1].charAt(1);
	}

	public static void toast(Activity a, String show) {
		Toast.makeText(a, show, Toast.LENGTH_LONG).show();
	}

	public static void esconderTeclado(EditText e) {
		InputMethodManager imm = (InputMethodManager) e.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(e.getWindowToken(), 0);
	}
}
