package com.lionseal.mm.viajantes;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.lionseal.mm.viajantes.database.MyDBHelper;
import com.lionseal.mm.viajantes.database.Query;
import com.lionseal.mm.viajantes.utils.SpinnerClientes;
import com.lionseal.mm.viajantes.utils.SpinnerItemCliente;
import com.lionseal.mm.viajantes.utils.Utiles;

public class SeleccionCliente extends Activity implements OnEditorActionListener {

	private Spinner spinnerClientes;
	private EditText editBuscar;
	public EditText editMail;

	private final static int ACTIVITY_ALTA_PEDIDO = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_seleccion_cliente);
		spinnerClientes = (Spinner) findViewById(R.id.spinner_clientes);
		editBuscar = (EditText) findViewById(R.id.edit_buscar_cliente);
		editBuscar.setOnEditorActionListener(this);
		editMail = (EditText) findViewById(R.id.edit_mail);
		setResult(0);
	}

	protected void onResume() {
		super.onResume();

		SharedPreferences settings = getSharedPreferences(AltaPedido.PREFS_ALTA_PEDIDO, 0);

		editBuscar.setText(settings.getString(AltaPedido.KEY_BUSQUEDA_CLIENTE, ""));
		int selected = settings.getInt(AltaPedido.KEY_CLIENTE_SELECTED, -1);
		if (selected != -1) {
			buscarCliente(null);
			spinnerClientes.setSelection(selected);
			spinnerClientes.setVisibility(View.VISIBLE);
			editMail.setText(settings.getString(AltaPedido.KEY_MAIL_CLIENTE, ""));
		}
	}

	protected void onPause() {
		super.onPause();
		SharedPreferences settings = getSharedPreferences(AltaPedido.PREFS_ALTA_PEDIDO, 0);
		SharedPreferences.Editor editor = settings.edit();

		editor.putString(AltaPedido.KEY_BUSQUEDA_CLIENTE, editBuscar.getText().toString());

		if (spinnerClientes.getVisibility() == View.VISIBLE) {
			SpinnerClientes adapter = (SpinnerClientes) spinnerClientes.getAdapter();
			editor.putInt(AltaPedido.KEY_CLIENTE_SELECTED, adapter.getPositionItemSelected());
			editor.putString(AltaPedido.KEY_MAIL_CLIENTE, editMail.getText().toString());
		}
		editor.commit();
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (actionId == 100) {
			buscarCliente(editBuscar);
			return true;
		}
		return false;
	}

	public void buscarCliente(View view) {
		MyDBHelper helper = new MyDBHelper(this);
		SQLiteDatabase db = helper.getReadableDatabase();

		ArrayList<SpinnerItemCliente> values;
		Cursor c;

		// Clientes
		String query = Query.get().clientes();

		query += " WHERE " + MyDBHelper.Clientes.razon_social + " LIKE '%" + editBuscar.getText().toString() + "%' ORDER BY "
				+ MyDBHelper.Clientes.razon_social;

		c = db.rawQuery(query, null);
		values = new ArrayList<SpinnerItemCliente>();
		if (c.moveToFirst()) {
			c.moveToPrevious();
			values.add(new SpinnerItemCliente("", "Seleccionar Cliente", false, ""));
			while (c.moveToNext()) {
				SpinnerItemCliente item = nuevoCliente(c);
				values.add(item);
			}
		} else {
			// No se encuentra ningun cliente
			values.add(new SpinnerItemCliente("", "Ning�n cliente encontrado", false, ""));
		}
		SpinnerClientes adapter = new SpinnerClientes(this, R.layout.spinner_row, values);
		spinnerClientes.setOnItemSelectedListener(adapter);
		spinnerClientes.setAdapter(adapter);
		spinnerClientes.setVisibility(View.VISIBLE);
		Utiles.esconderTeclado(editBuscar);

		db.close();
	}

	private SpinnerItemCliente nuevoCliente(Cursor c) {
		return new SpinnerItemCliente(c.getString(c.getColumnIndex(MyDBHelper.Clientes.idespecial)), c.getString(
				c.getColumnIndex(MyDBHelper.Clientes.razon_social)).trim(), false, c.getString(c
				.getColumnIndex(MyDBHelper.Clientes.mail)));
	}

	public void realizarPedido(View v) {
		SpinnerClientes adapter = (SpinnerClientes) spinnerClientes.getAdapter();
		SpinnerItemCliente item = (SpinnerItemCliente) adapter.getItemSelected();
		if (item == null) {
			Toast.makeText(this, "Ning�n cliente seleccionado", Toast.LENGTH_LONG).show();
			return;
		}

		// crear nuevo registro ventas
		registrarPedido(item.getId(), item.getDescripcion(), editMail.getText().toString());

		Toast.makeText(this, "Cliente seleccionado: " + item.getDescripcion(), Toast.LENGTH_SHORT).show();

		Intent i = new Intent(this, AltaPedido.class);
		startActivityForResult(i, ACTIVITY_ALTA_PEDIDO);
	}

	private void registrarPedido(String idcliente, String descripcioncliente, String mail) {
		MyDBHelper helper = new MyDBHelper(this);
		SQLiteDatabase db = helper.getWritableDatabase();

		ContentValues values = new ContentValues();

		String fecha = getFecha();
		String hora = getHora();
		String idvendedor = getIdVendedor();
		String idespecial = getIdEspecial(idvendedor, fecha, hora);

		values.put(MyDBHelper.Ventas.idespecial, idespecial);
		values.put(MyDBHelper.Ventas.idvendedor, idvendedor);
		values.put(MyDBHelper.Ventas.idcliente, idcliente);
		values.put(MyDBHelper.Ventas.fecha, fecha);
		values.put(MyDBHelper.Ventas.hora, hora);
		values.put(MyDBHelper.Ventas.total, 0);
		values.put(MyDBHelper.Ventas.vuelto, AltaPedido.KEY_PEDIDO_ABIERTO);

		db.insert(MyDBHelper.Ventas.TABLE_NAME, null, values);

		String where = MyDBHelper.Clientes.idespecial + "='" + idcliente + "'";
		values = new ContentValues();
		values.put(MyDBHelper.Clientes.mail, mail);
		
		db.update(MyDBHelper.Clientes.TABLE_NAME, values, where, null);

		SharedPreferences settings = getSharedPreferences(AltaPedido.PREFS_ALTA_PEDIDO, 0);
		SharedPreferences.Editor editor = settings.edit();

		editor.putString(AltaPedido.KEY_ID_ESPECIAL, idespecial);
		editor.putString(AltaPedido.KEY_RAZON_SOCIAL_CLIENTE, descripcioncliente);
		editor.commit();

		db.close();
	}

	private String getFecha() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(Calendar.getInstance().getTime());
	}

	private String getHora() {
		SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
		return df.format(Calendar.getInstance().getTime());
	}

	private String getIdVendedor() {
		SharedPreferences settings = getSharedPreferences(MainActivity.PREFS_LOCALES, 0);
		return settings.getString(MainActivity.KEY_ID_VENDEDOR, "");
	}

	private String getIdEspecial(String idvendedor, String fecha, String hora) {
		String id = agregarCeros(idvendedor, 3);
		String[] f = fecha.split("-");
		String[] h = hora.split(":");
		String date = f[0] + f[1] + f[2] + h[0] + h[1] + h[2];
		return "C" + id + date;
	}

	private String agregarCeros(String s, int longitudRequerida) {
		int longitud = s.length();
		while (longitud < longitudRequerida) {
			s = "0" + s;
			longitud++;
		}
		return s;
	}

	public void onActivityResult(int code, int res, Intent data) {
		if (code == ACTIVITY_ALTA_PEDIDO) {
			if (res == 0) {
				setResult(0);
				finish();
				return;
			}
			setResult(1);
			finish();
		}
	}
}
