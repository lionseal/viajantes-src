package com.lionseal.mm.viajantes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.lionseal.mm.viajantes.database.MyDBManager;
import com.lionseal.mm.viajantes.database.MyDBTransferencia;
import com.lionseal.mm.viajantes.utils.Utiles;

public class MainActivity extends Activity implements OnClickListener{

	public final static int ERROR_CONEXION = -100;
	public final static int VENDEDOR_INVALIDO = -200;
	public final static int ERROR_TRANSFERENCIA = -300;
	public final static int NO_HAY_PEDIDOS = -400;
	public final static int CANCELADO = 0;
	public final static int EXITO_ACTUALIZAR = 100;
	public final static int EXITO_TRANSFERENCIA = 200;

	private final static int ACTIVITY_ALTA_PEDIDO = 10;

	// Shared Preferences KEYS configuracion
	public static final String PREFS_LOCALES = "prefLocales";
	public static final String KEY_ID_VENDEDOR = "idVendedor";
	public static final String KEY_BANDERA_ORIGEN_DATOS = "banderaOrigenDatos";
	public static final String KEY_BORRAR_DATOS = "9876543210";
	public static final String KEY_RAZON_SOCIAL_VENDEDOR = "razonSocialVendedor";

	public static final String KEY_TRANSACCIONES_PENDIENTES = "transaccionesPendientes";

	private String razon_social_vendedor, id_vendedor;
	private int transacciones_pendientes;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void clickAltaPedido(View view) {
		SharedPreferences settings = getSharedPreferences(AltaPedido.PREFS_ALTA_PEDIDO, 0);
		String idespecial = settings.getString(AltaPedido.KEY_ID_ESPECIAL, null);
		if (idespecial == null) {
			Intent intent = new Intent(this, SeleccionCliente.class);
			startActivityForResult(intent, ACTIVITY_ALTA_PEDIDO);
		} else {
			Intent intent = new Intent(this, AltaPedido.class);
			startActivityForResult(intent, ACTIVITY_ALTA_PEDIDO);
		}
	}

	public void clickTransferirPedido(View view) {
		if (transacciones_pendientes > 0) {
			MyDBTransferencia transfer = new MyDBTransferencia(this);
			transfer.transferirPedidos();
		} else {
			bdError(NO_HAY_PEDIDOS);
		}
	}

	public void clickActualizarBD(View view) {
		if (transacciones_pendientes > 0) {
			mostrarAdvertencia();
		} else {
			actualizarBD();
		}
	}

	public void clickConfiguracion(View view) {
		Intent intent = new Intent(this, Configuracion.class);
		startActivity(intent);
	}
	
	private void actualizarBD(){
		if (!id_vendedor.equals("")) {
			MyDBManager actualizador = new MyDBManager(this);
			actualizador.actualizarBD();
			getSharedPreferences(AltaPedido.PREFS_ALTA_PEDIDO, 0).edit().clear().commit();
			getSharedPreferences(PREFS_LOCALES, 0).edit().putInt(KEY_TRANSACCIONES_PENDIENTES, 0).commit();
		} else {
			bdError(VENDEDOR_INVALIDO);
		}
	}

	private void cargarDatos() {
		SharedPreferences settings = getSharedPreferences(PREFS_LOCALES, 0);
		razon_social_vendedor = settings.getString(KEY_RAZON_SOCIAL_VENDEDOR, "[Razon_Social_Vendedor]");
		id_vendedor = settings.getString(KEY_ID_VENDEDOR, "");
		transacciones_pendientes = settings.getInt(KEY_TRANSACCIONES_PENDIENTES, 0);
	}

	private void actualizarTextos() {
		cargarDatos();
		TextView text = (TextView) findViewById(R.id.text_razon_social_vendedor);
		text.setText(razon_social_vendedor);
		text = (TextView) findViewById(R.id.text_id_vendedor);
		text.setText("ID: " + id_vendedor);
		text = (TextView) findViewById(R.id.text_num_trans);
		text.setText("" + transacciones_pendientes);
	}

	public void actualizacionExitosa() {
		actualizarTextos();
	}

	public void transferenciaExitosa() {
		getSharedPreferences(PREFS_LOCALES, 0).edit().putInt(KEY_TRANSACCIONES_PENDIENTES, 0).commit();
		actualizarTextos();
	}

	public void bdError(int err) {
		switch (err) {
		case ERROR_CONEXION:
			Utiles.toast(this, "Error al conectar con Base de Datos");
			break;
		case CANCELADO:
			Utiles.toast(this, "Acci�n cancelada");
			break;
		case VENDEDOR_INVALIDO:
			Utiles.toast(this, "Vendedor inv�lido");
			break;
		case ERROR_TRANSFERENCIA:
			Utiles.toast(this, "Error al transferir el/los pedido(s)");
			break;
		case NO_HAY_PEDIDOS:
			Utiles.toast(this, "Debe existir al menos un pedido para transferir");
			break;
		default:
			Utiles.toast(this, err + "");
			break;
		}
		actualizarTextos();
	}
	
	private void mostrarAdvertencia(){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle("Actualizar Base de Datos...");
		alertDialog.setMessage("Si actualiza la Base de Datos perdera los pedidos sin transferir, desea continuar?");
		alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
		alertDialog.setPositiveButton("Si", this);
		alertDialog.setNegativeButton("No", this);
		alertDialog.show();
	}

	protected void onResume() {
		super.onResume();
		actualizarTextos();
	}

	public void onActivityResult(int code, int res, Intent data) {
		if (code == ACTIVITY_ALTA_PEDIDO) {
			if (res == 0) {
				return;
			}
			transacciones_pendientes++;
			SharedPreferences settings = getSharedPreferences(PREFS_LOCALES, 0);
			settings.edit().putInt(KEY_TRANSACCIONES_PENDIENTES, transacciones_pendientes).commit();
		}
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		switch (which) {
		case DialogInterface.BUTTON_POSITIVE:
			actualizarBD();
			break;
		case DialogInterface.BUTTON_NEGATIVE:
			break;
		default:
			break;
		}
	}
}
